<?php
	define("MODULE", true);
	error_reporting(E_ALL);

	session_start();

	require_once("./.config.php");
	require_once("./.functions.php");

	require_once("./lib/Template.class.php");
	require_once("./lib/Language.class.php");
	require_once("./lib/Database.class.php");
	require_once("./lib/User.class.php");

	try {
		$db = new Database($_PH_CONFIG['SQL_PATH'], $_PH_CONFIG['SQL_USER'], $_PH_CONFIG['SQL_PASS'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8") );
	} catch (PDOException $e) {
		die("DB Error!");
	}

	$lng = new Lang($_PH_CONFIG['DEFAULT_LANGUAGE'], $_PH_CONFIG['BASE_PATH'].'/res/language');

	$usr = new User($db);
	$_USER = $usr->get();


// TODO:: LOAD FROM DATABASE
	$mod = (isset($_REQUEST['mod']) && file_exists("./mod/mod_".$_REQUEST['mod'].".php")?$_REQUEST['mod']:'main');

// TODO: Find a better way to set this
	$_MENU = array(
		$lng->{'Settings'} => '?mod=settings',
		$lng->{'Messages'} => '?mod=search',
		$lng->{'Search'} => '?mod=messages',
		$lng->{'Help'} => '?mod=help',
	);

	$menu = '<ul>';
	foreach($_MENU as $mk => $mv) 
		$menu.= sprintf('<li %s><a href="%s">%s</a></li>', strcasecmp($mk, $mod) == 0?"style='color: red;'":'', $mv, $mk);
	$menu.= '</ul>';

	$tpl = new Template($_PH_CONFIG['TEMPLATE_PATH'], $_PH_CONFIG, $lng, $_USER);
	$tpl->load('main');
	$tpl->set("title", "InsectBB by InsecureBG.org");
	$add_scripts = "<script type='text/javascript' src='res/scripts/notification_popup.js'></script>\n";
	$add_scripts.= "<script type='text/javascript' src='res/scripts/bb_tags.js'></script>\n";
	$tpl->set("custom_headers", $add_scripts);
	$tpl->set("user_menu", $menu);
	$tpl->set("footer", "<a href=\"https://github.com/idimiter/insectBB/\" target=\"_new\">InsectBB</a> v0.3a 2011 &copy; <a href=\"http://insecurebg.org\" target=\"_blank\">InsecureBG.org</a>");

	$NAVLINE = get_navline($db, $_PH_CONFIG, $lng);

	ob_start();
	// TODO:: Sanity checks for blahblag.php/../ LFI
	require_once("./mod/mod_".$mod.".php");
	require_once("./.notification.php");
	$out_buffer = ob_get_clean();

	$tpl->set("body", $out_buffer);
	$tpl->set("navline", $NAVLINE);
	$tpl->output();

// Cleaning up
	$tpl = null;
	$db = null;
	$lng = null;
