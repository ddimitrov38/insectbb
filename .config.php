<?php
	$_PH_CONFIG = array();

// Auto
	$_PH_CONFIG['BASE_PATH'] = dirname(__FILE__);

// Manual
	$_PH_CONFIG['BASE_URL'] = 'http://localhost/insectBB';
	$_PH_CONFIG['DEFAULT_TEMPLATE'] = 'default';
	$_PH_CONFIG['TEMPLATE_PATH'] = $_PH_CONFIG['BASE_PATH'].'/res/template/'.$_PH_CONFIG['DEFAULT_TEMPLATE'];
	$_PH_CONFIG['SQL_PATH'] = 'sqlite:../phorum.db';
//	$_PH_CONFIG['SQL_PATH'] = 'mysql:host=127.0.0.1;dbname=phorum';
	$_PH_CONFIG['SQL_USER'] = 'root';
	$_PH_CONFIG['SQL_PASS'] = '';

	$_PH_CONFIG['DEFAULT_LANGUAGE'] = 'en';

// Forum specific
	$_PH_CONFIG['POSTS_PER_PAGE'] = 10;

// Emoticons
	$_PH_CONFIG['EMOTICON_CODES'] = array(
		':-)' => '&#9786;',
		':-(' => '&#9785;',
		':hearts:' => '&hearts;',
		':rain:' => '&#9730;',
		':star:' => '&#9734;',
		':phone:' => '&#9743;',
		':point:' => '&#9756;',
		':skull:' => '&#9760;',
		':radioactive:' => '&#9762;',
		':biohazard:' => '&#9763;',
		':caduceus:' => '&#9764;',
		':ankh:' => '&#9765;',
		':cccp:' => '&#9773;',
		':yinyang:' => '&#9775;',
		':moon:' => '&#9790;',
		':venus:' => '&#9792;',
		':mars:' => '&#9794;',
		':misical note:' => '&#9835;',
		':cut here:' => '&#9986;'
	);
