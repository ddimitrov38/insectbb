function add_bb_tag(tag) {
	var area = document.getElementById("content_area");

	if (!area)
		return -1;

	var begin_tag = '[' + tag + ']';
	var end_tag = '[/' + tag + ']';

	var start_selection = area.selectionStart;
	var end_selection = area.selectionEnd;

	var text_before = area.value.substr(0, start_selection);
	var text_selection = area.value.substr(start_selection, end_selection - start_selection);
	var text_after = area.value.substr(end_selection);


	area.value = text_before + begin_tag + text_selection + end_tag + text_after;

	area.focus();

	return 0;
}

function add_emoticon(code) {
	var area = document.getElementById("content_area");

	if (!area)
		return -1;

	var start_selection = area.selectionStart;
	var end_selection = area.selectionEnd;

	var text_before = area.value.substr(0, start_selection);
	var text_selection = area.value.substr(start_selection, end_selection - start_selection);
	var text_after = area.value.substr(end_selection);


	area.value = text_before + code + text_after;

	area.focus();

	return 0;
}
