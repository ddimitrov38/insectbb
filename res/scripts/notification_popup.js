var MSG_INFO = 0;
var MSG_WARNING = 1;
var MSG_ERROR = 2;

// TODO: Customize dialogs in CSS
function popup_message(mtype, message) {
	var dialog = document.createElement("div");
	dialog.id = "popup-message-dialog";
	dialog.className = "popup-message-dialog-show";

	var icons = new Array('info', 'warning', 'error');
	var colors = new Array('silver', 'yellow', 'red');

	dialog.style.color =  colors[mtype];
	dialog.style.backgroundImage = "url(res/images/msg-" + icons[mtype] + ".png)";
	dialog.style.backgroundRepeat = "no-repeat";

	dialog.appendChild(document.createTextNode(message));

	dialog.onclick = animate_close;
	document.body.appendChild(dialog);

	setTimeout("animate_close()", 5000);
}

function animate_close() {
	var dialog = document.getElementById("popup-message-dialog");

	if (dialog)
		dialog.className = "popup-animate-close";

	setTimeout("popup_close()", 1000);
}

function popup_close() {
	var dialog = document.getElementById("popup-message-dialog");

	if (dialog)
		document.body.removeChild(dialog);
}
