<!--DOCTYPE HTML-->
<html>
<head>
	<title>{TITLE}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="res/template/default/styles/default.css" />
	<link rel="stylesheet" type="text/css" href="res/template/default/styles/forum-tables.css" />
	<link rel="stylesheet" type="text/css" href="res/template/default/styles/other.css" />
	<link rel="stylesheet" type="text/css" href="res/template/default/styles/animations.css" />
	<link rel="icon" href="res/template/default/img/favicon.ico" type="image/vnd.microsoft.icon" />
	{CUSTOM_HEADERS}
</head>

<body>
<table class="header" cellspacing="0" cellpadding="0">
<tr>
	<td id="logo">
<!-- 	Logo -->
	<a href="{CONFIG:BASE_URL}"><img src="res/template/default/img/spider.png" border="0" alt="{FORUM_TITLE}" /></a>
<!-- 	END Logo -->
	</div>

	<td style="width: 160px;text-align: right; padding-right: 10px; vertical-align: bottom;padding-bottom: 15px;">
<!-- 	Quick login -->
		{IF:LOGGED}{LANG:Logged in as} <b>{USER:USERNAME}</b> [<a href="?mod=user&action=logout" title="{LANG:Logout}">x</a>]{ELSE}{INCLUDE:quick_login}{/IF}
<!-- 	END Quick login -->
	</td>
</tr>
</table>

<div class="menu-bar">
	{USER_MENU}
</div>

<div class="menu-bar">
	<div class="navline">{NAVLINE}</div>
</div>

