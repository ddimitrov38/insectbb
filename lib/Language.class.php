<?php
/**
 * Simple language class
 * 
 * !package Language
 * !author Dimiar Dimitrov
 * !email mitko@insecurebg.org
 * !date 11.17.2011
 *
*/
class Lang {
	var $lang = null;
	var $file = null;
	var $words = array();

	function __construct($lng, $path = '.') {
		$file = $path.'/'.$lng.'.lang';

		if (!file_exists($file))
			return -1;

		$this->lang = $lng;
		$this->file = $file;
		$this->load();
	}


	function load() {
		if (!$this->file)
			return -1;

		$f = file($this->file);

		foreach($f as $l) {
			if (preg_match("/(.+?)\s*?=\s*(.+)/s", $l, $m)) {
				$this->words[$m[1]] = $m[2];
			}
		}
	}

	function __get($p) {
			if ( array_key_exists($p, $this->words) )
				return $this->words[$p];

			return $p;
	}

}
