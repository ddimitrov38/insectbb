<?php
// by PxL

class User {
	var $db;

	function __construct($db) {
		$this->db = $db;

		return 0;
	}

	function login($username, $password) {
		if (!$this->db)
			return -1;

		$found_user = $this->db->q("SELECT id, user, pass, email FROM USER WHERE user = ?", $username);

		if (count($found_user) > 0 && $found_user[0]['pass'] === crypt(sha1($password, true), $found_user[0]['pass'])) {
			$_SESSION['user'] = array(
				'id' => $found_user[0]['id'],
				'username' => $found_user[0]['user'],
				'email' => $found_user[0]['email']
			);

			return true;
		} else 
			return false;
	}

	function get() {
		if (!array_key_exists('user', $_SESSION) || !is_array($_SESSION['user']))
			return false;

		return $_SESSION['user'];
	}
}
