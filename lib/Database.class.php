<?php
class Database extends PDO {
	function q() {
			if (func_num_args() < 1)
				return -1;

			$args = func_get_args();
			$fstring = array_shift($args);

			if (! ($sth = $this->prepare($fstring)) ) 
				return -2;

			$sth->execute($args);

			return $sth->fetchAll();
	}

	function e() {
			if (func_num_args() < 1)
				return -1;

			$args = func_get_args();
			$fstring = array_shift($args);

			if (! ($sth = $this->prepare($fstring)) ) 
				return -2;

			$sth->execute($args);

			return $sth->errorCode()!=0?$sth->errorInfo():$sth->rowCount();
	}
}
