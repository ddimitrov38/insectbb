<?php
/**
 * Simple template class
 * 
 * !package Template
 * !author Dimiar Dimitrov
 * !email mitko@insecurebg.org
 * !date 11.07.2011
 *
*/

class Template {
	var $path;
	var $content;
	var $config;
	var $lang;
	var $user;

	function __construct($path, $config = array(), $lang = null, $user = array()) {
		$this->path = $path;
		$this->config = $config;
		$this->lang = $lang;
		$this->user = $user;
	}

// Callbacks
	private function user_var_cb($m) {
		$user = strtolower($m[1]);
		return (array_key_exists($user, $this->user)?$this->user[$user]:$user);
	}

	private function config_var_cb($m) {
		if (array_key_exists($m[1], $this->config))
			return $this->config[$m[1]];

			return $m[1]; 
	}

	private function include_file_cb($m) {
		$file = $this->path.'/'.$m[1].'.tpl';

		if ($this->load($m[1]) === 0)
				return $this->content;

			return $m[1]; 
	}

	private function if_condition_cb($m) {
		switch($m[1]) {
			case 'LOGGED':
				return is_array($this->user)?$m[2]:$m[3];
			break;
		}

		return '';
	}

	private function lang_var_cb($m) {
			return ($this->lang)?$this->lang->$m[1]:$m[1];
	}
// END Callbacks

	function load($name) {
		$file = $this->path.'/'.$name.'.tpl';

		if (!file_exists($file))
			return -1;

		$this->content = file_get_contents($file);

// TODO: Unite in one call
		$this->content = preg_replace_callback('/\{IF:(.*?)\}(.*?){ELSE}(.*?){\/IF}/si', array('Template', 'if_condition_cb'), $this->content);
		$this->content = preg_replace_callback('/\{INCLUDE:(.*?)\}/si', array('Template', 'include_file_cb'), $this->content);
		$this->content = preg_replace_callback('/\{CONFIG:(.*?)\}/si', array('Template', 'config_var_cb'), $this->content);
		$this->content = preg_replace_callback('/\{USER:(.*?)\}/si', array('Template', 'user_var_cb'), $this->content);
		$this->content = preg_replace_callback('/\{LANG:(.*?)\}/si', array('Template', 'lang_var_cb'), $this->content);

		return 0;
	}
/**
 * Sets a template variable $key with the value of $val
 * !param key 
**/
	function set($key, $val) {
			$this->content = str_ireplace('{'.$key.'}', $val, $this->content);
	}

	function output() {
			echo $this->content;
	}
}
