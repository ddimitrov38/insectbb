<?php
	if (isset($_SESSION['notice'])) {
		printf("<script type='text/javascript'>popup_message(MSG_INFO, '%s');</script>", $_SESSION['notice']);
		$_SESSION['notice'] = null;
	} else if (isset($_SESSION['warning'])) {
		printf("<script type='text/javascript'>popup_message(MSG_WARNING, '%s');</script>", $_SESSION['warning']);
		$_SESSION['warning'] = null;
	} else if (isset($_SESSION['error'])) {
		printf("<script type='text/javascript'>popup_message(MSG_ERROR, '%s');</script>", $_SESSION['error']);
		$_SESSION['error'] = null;
	}
