<?php
function merror($msg) {
	printf('<div class="center-me"><div class="error-message">%s</div></div>', $msg);
}

function parse_post($msg, $cfg) {
	return parse_emoticons(parse_bb_tags(htmlspecialchars($msg)), $cfg);
}

function icons_map($icon) {
	return '<span style="font-size: 25px;">'.$icon.'</span>';
}
function parse_emoticons($msg, $cfg) {
	$patterns = array_keys($cfg['EMOTICON_CODES']);
	$replaces = array_values(array_map("icons_map", $cfg['EMOTICON_CODES']));

	return str_replace($patterns, $replaces, $msg);
}

function parse_bb_tags($msg) {
	$single_tags = array(
//			"[code]", "[/code]",
			"[sql]", "[/sql]",
			"[quote]", "[/quote]",
			"[center]", "[/center]",
			"[b]", "[/b]",
			"[i]", "[/i]",
			"[u]", "[/u]"
	);
	$single_tags_replace = array(
//		"<fieldset class='bb_code'><legend>CODE</legend><pre>","</pre></fieldset>",
		"<fieldset class='bb_sql'><legend>SQL</legend>", "</fieldset>",
		"<fieldset class='bb_quote'><legend>QUOTE</legend>", "</fieldset>",
		"<p style='text-align: center;'>", "</p>",
		"<b>", "</b>",
		"<i>", "</i>",
		"<u>", "</u>"
	);

	$complex_tags = array(
		"/\[quote=(.+?)\](.+)\[\/quote\]/si",
		"/\[url=(.+?)\](.+?)\[\/url\]/si"
	);
	$complex_tags_replace = array(
		"<fieldset class='bb_quote'><legend><i>\\1</i></legend>\\2</fieldset>",
		"<a href='\\1' target='_new'>\\2</a>"
	);

	$callback_tags = array(
		"/\[(code)\](.+?)\[\/code\]/si"
	);


	$msg = preg_replace_callback($callback_tags, 'replace_callback', $msg);

	$msg = preg_replace($complex_tags, $complex_tags_replace, $msg);

	return str_ireplace($single_tags, $single_tags_replace, $msg);
}


function replace_callback($matches) {
	if (!is_array($matches))
		return $matches;

	if (strtolower($matches[1]) == 'code')
		return sprintf("<fieldset class='bb_code'><legend>CODE</legend>%s</fieldset>", $matches[2]);

	return $matches[0];
}


function pagging($addr, $total_pages, $current_page) {
	$start_pg = 1;
	echo '<td class="pagging">';
	if ($current_page > 2) {
		$start_pg = $current_page - 1;
		printf('<a href="%s">&laquo; first</a>&nbsp;', $addr);
		echo '...&nbsp;';
	}

	for ($i = $start_pg; $i < $start_pg + 5; $i++) {
		if ($i > $total_pages)
				break;

		if ($i == $current_page + 1)
			printf('<span style="color: #555">[</span>%d<span style="color: #555">]</span>&nbsp;', $i);
		else
			printf('<a href="%s&page=%d">%d</a>&nbsp;', $addr, $i, $i);
	}

	if ($start_pg + 4 < $total_pages) {
		echo '...&nbsp;';
		printf('<a href="%s&page=%d">last &raquo;</a>', $addr, $total_pages);
	}
	echo '</td>';
}


function get_navline($db, $conf, $lng) {
	$navline = sprintf('<a href="%s">%s</a>', $conf['BASE_URL'], $lng->{'Forums'});

	if (isset($_REQUEST['mod']) && $_REQUEST['mod'] == 'forum' && isset($_REQUEST['f'])) {
		$fid = (int)$_REQUEST['f'];

		$forum_r = $db->q("SELECT title FROM forum WHERE id = ?", $fid);
		if ($forum_r) 
			$navline.= sprintf('&nbsp;&raquo;&nbsp;<a href="%s?mod=forum&f=%d">%s</a>', $conf['BASE_URL'], $fid, $forum_r[0][0]);
	}

	if (isset($_REQUEST['mod']) && $_REQUEST['mod'] == 'topic' && isset($_REQUEST['t'])) {
		$tid = (int)$_REQUEST['t'];

		$forum_r = $db->q("SELECT f.id as forum_id, f.title as forum, t.title as topic FROM forum f, topic t WHERE f.id = t.forum_id AND t.id = ?", $tid);
		if ($forum_r) { 
			$navline.= sprintf('&nbsp;&raquo;&nbsp;<a href="%s?mod=forum&f=%d">%s</a>', $conf['BASE_URL'], $forum_r[0]['forum_id'], $forum_r[0]['forum']);
			$navline.= sprintf('&nbsp;&raquo;&nbsp;<a href="%s?mod=topic&t=%d">%s</a>', $conf['BASE_URL'], $tid, $forum_r[0]['topic']);
		}
	}


	return $navline;
}
