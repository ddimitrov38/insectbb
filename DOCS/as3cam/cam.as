/*
 * by Dimitar Dimitrov <mitko@insecurebg.org>
 * Sofia, Bulgaria 2012
 *
 * as3compile -T10 -N cam.as
 *
 * 10x to abozhilov for the flash version compilation settings
 */

package {
	import flash.display.*;
	import flash.net.*;
	import flash.media.Camera;
	import flash.media.Video;
	import flash.media.Sound;
	import flash.events.MouseEvent;
	import flash.events.ActivityEvent;
	import flash.utils.ByteArray;
	import flash.geom.Rectangle;
	import com.adobe.images.PNGEncoder;
	import org.insecurebg.buttons.CameraButton;

	public class AvatarCamera extends MovieClip {
		private var video:Video;
		private var cameraButton:CameraButton = new CameraButton(3, 3, "cam.png");
		private var okButton:CameraButton = new CameraButton(100, 170, "ok.png");
		private var cancelButton:CameraButton = new CameraButton(153, 170, "cancel.png");
		private var saveButton:CameraButton = new CameraButton(206, 170, "save.png");
		private var shutter:Sound = new Sound();
		private var byteArray:ByteArray;

		public function AvatarCamera() {
			var camera:Camera = Camera.getCamera();

			if (camera != null) {
				shutter.load(new URLRequest("shutter.mp3"));

				video = new Video(320, 240);
				video.attachCamera(camera);
				video.smoothing = true;

				camera.addEventListener(ActivityEvent.ACTIVITY, activityHandler);

				addChild(video);
			} else {
				trace("No camera detected\n");
			}
		}

		private function showMenu() {
			addChild(okButton);
			addChild(cancelButton);
			addChild(saveButton);

			removeChild(video);
			removeChild(cameraButton);
		}

		private function hideMenu() {
			removeChild(okButton);
			removeChild(cancelButton);
			removeChild(saveButton);

			addChild(video);
			addChild(cameraButton);
		}

		private function activityHandler(event:ActivityEvent):void {
			if (!event.activating)
				return;

			addChild(cameraButton);
			cameraButton.addEventListener(MouseEvent.CLICK, cameraClick);
			
			okButton.addEventListener(MouseEvent.CLICK, okClick);
			cancelButton.addEventListener(MouseEvent.CLICK, cancelClick);
			saveButton.addEventListener(MouseEvent.CLICK, saveClick);
		}

		private function cameraClick(m:MouseEvent):void {
			trace('Say cheese!\n');

			shutter.play();

			var bitmapData:BitmapData = new BitmapData(320, 240);

			bitmapData.draw(video);

			var encoder:PNGEncoder = new PNGEncoder();
			byteArray = encoder.encode(bitmapData)

			var shot:Bitmap = new Bitmap(bitmapData);

			addChild(shot);
			showMenu();
		}

		private function okClick(m:MouseEvent):void {
			var _request:URLRequest = new URLRequest ("http://localhost/readimage.php");
			var loader: URLLoader = new URLLoader();

			_request.contentType = "application/octet-stream";
			_request.method = URLRequestMethod.POST;
			_request.data = byteArray;

			loader.load(_request);

			hideMenu();
		}

		private function cancelClick(m:MouseEvent):void {
			hideMenu();
		}

		private function saveClick(m:MouseEvent):void {
			var file:FileReference = new FileReference();

			file.save(byteArray, "test.png");

			hideMenu();
		}
	}
}

