package org.insecurebg.buttons {
	import flash.display.*;
	import flash.net.*;

	public class ButtonImgDisplayState extends Sprite {

		public function ButtonImgDisplayState(x:Number, y:Number, img:String, _alpha:Number) {
			var my_loader : Loader = new Loader();
			my_loader.load(new URLRequest(img));
			addChild(my_loader);

			this.alpha = _alpha;
			this.x = x;
			this.y = y;
		}
	}

	public class CameraButton extends SimpleButton {
		private var upAlpha : Number = 0.5;
		private var overAlpha : Number = 0.7;

		public function CameraButton(x : Number, y : Number, img : String, upState : DisplayObject = null, overState : DisplayObject = null, downState : DisplayObject = null, hitTestState : DisplayObject = null) {
			upState = new ButtonImgDisplayState(x, y, img, upAlpha);
			overState = new ButtonImgDisplayState(x, y, img, overAlpha);
			downState = new ButtonImgDisplayState(x, y, img, upAlpha);
			hitTestState = new ButtonImgDisplayState(x, y, img, upAlpha);

			super(upState, overState, downState, hitTestState);
		}
	}
}
