<?php
	defined("MODULE") or die(42);

	$fid = isset($_REQUEST['f'])?(int)$_REQUEST['f']:0;
	$act = isset($_REQUEST['action'])?$_REQUEST['action']:'default';

	switch ($act) {
		case 'new':
			if (!$_USER) {
				merror($lng->{'Not logged in'});
				break;
			}

			require_once("mod_forum/new_topic.php");
		break;

		default:
			require_once("mod_forum/view_forum.php");
		break;
	}
