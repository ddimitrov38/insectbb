<?php
	defined('MODULE') or die(42);

	echo '<table class="forum-tbl">';

	foreach($db->query('SELECT id, title FROM section ORDER BY position ASC;') as $section) {
			printf('<tr><td class="forum-section-head" colspan="4">%s</td></tr>', $section['title']);
			echo '<tr>';
			$q = sprintf('
					SELECT 
						f.id, 
						f.title, 
						f.description, 
						COUNT(DISTINCT t.id) as count_topic, 
						COUNT(p.id) as count_post 
					FROM  
						forum f 
					LEFT JOIN 
						topic t 
					ON 
						(t.forum_id = f.id) 
					LEFT JOIN 
						post p 
					ON 
						(p.topic_id = t.id) 
					WHERE 
						section_id = \'%d\' 
					GROUP BY
						f.id
					ORDER BY 
						f.position ASC;
					', $section['id']
			);

		printf('
				<tr>
					<th width="*">%s</th>
					<th width="7%%">%s</th>
					<th width="7%%">%s</th>
					<th width="25%%">%s</th>
				</tr>',
				$lng->Forum,
				$lng->Topics,
				$lng->Posts,
				$lng->{'Last post'}
		);

			foreach($db->query($q) as $forum) {

				if ($forum['count_post'] > 0) {
					$sth = $db->prepare("SELECT t.id as topic_id, u.user, t.title, u.id as user_id, p.created_date FROM post p LEFT JOIN user u ON (u.id = p.user_id) LEFT JOIN topic t ON (p.topic_id = t.id)  WHERE t.forum_id = ? ORDER BY p.created_date DESC LIMIT 1;");
					$sth->execute(array($forum['id']));
					$last_post_r = $sth->fetchAll();

					$forum['last_post'] = sprintf("<i>%s</i><br />Topic: <a href=\"?mod=topic&t=%d\">%s</a><br />by <a href='?mod=user&id=%d'>%s</a>", 
								date("Y-m-d H:i:s", $last_post_r[0]['created_date']),
								$last_post_r[0]['topic_id'],
								$last_post_r[0]['title'],
								$last_post_r[0]['user_id'],
								$last_post_r[0]['user']
					);
				} else 
					$forum['last_post'] = $lng->{'No posts'};

				printf('
				<tr>
					<td><a href="?mod=forum&f=%d">%s</a><div>%s</div></td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
				</tr>', 
				$forum['id'],
				$forum['title'],
				$forum['description'],
				$forum['count_topic'],
				$forum['count_post'],
				$forum['last_post']
			);
			}
			echo '</tr>';
	}

	echo '</table>';
