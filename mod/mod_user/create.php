<?php
	defined("MODULE") or die(42);

	if (isset($_POST['registration_submit'])) {

		if (strlen($_POST['do_not_fill']) > 0) {
			// TODO: Ban user, most probably a spam bot
			break;
		}

		$i = -1;
		while($i++) { // Dummy loop to 1 to go level 2 in order to use breaks

			$username = htmlspecialchars(substr(trim($_POST['username']), 0, 32));
// Check username
			if (strlen($username) < 3) {
				$error = $lng->{'Username too short'};
				break;
			}
			list(list($user_check)) = $db->q("SELECT COUNT(id) as cnt FROM user WHERE user = ?", $username);
			if ($user_check > 0) {
				$error = $lng->{'User already exists'};
				break;
			}

// Check passwords
			if ($_POST['password'] != $_POST['password_again']) {
				$error = $lng->{'Passwords do not match'};
				break;
			}

// Check email
			if (!preg_match("/[а-зa-z0-9!#$%&'*+\/_{|}~-]+(?:\.[а-зa-z0-9!#$%&'*+\/^_{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[a-z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|)\b/i", $_POST['email'])) {
				$error = $lng->{'Incorrect E-mail'};
				break;
			}

// Check registration IP
			list(list($ip_check)) = $db->q("SELECT COUNT(id) as cnt FROM user WHERE created_date < ? AND created_ip = ?", strtotime("now() - 1 month"), $_SERVER['REMOTE_ADDR']);
			if ($ip_check >= 3) {
				$error = $lng->{'Maximum 3 registrations from IP per month'};
				break;
			}

			$avatar = '';
			if ($_FILES['avatar']['size'] > 0) {
				$av_orig = @imagecreatefromstring(file_get_contents($_FILES['avatar']['tmp_name']));

				if ($av_orig === false) {
					$error = $lng->{'Invalid image type.'};
					break;
				}

				list($width, $height) = getimagesize($_FILES['avatar']['tmp_name']);
				$av = imagecreatetruecolor(96, 96);
				imagealphablending($av, false);
				imagesavealpha($av, true);
	
				imagecopyresampled($av, $av_orig, 0, 0, 0, 0, 96, 96, $width, $height);
	
				if ($tmpname = tempnam("/tmp", 'FAV')) {
					imagepng($av, $tmpname);
					list(, $avatar) = unpack("H*", file_get_contents($tmpname));
					unlink($tmpname);
				}
			}

			$salt = ((CRYPT_BLOWFISH === 1)?'$2a$08$':'').substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()))), 0, 22);
			$password = crypt(sha1($_POST['password'], true), $salt);
			$email = htmlspecialchars($_POST['email']);

			$db->q("INSERT INTO user (user, pass, email, avatar, created_date) VALUES (?, ?, ?, ?, ?)", $username, $password, $email, $avatar, time());
			$usr->login($username, $password);
		} // end while

		if (isset($error))
			$_SESSION['error'] = $error;
		else {
			$_SESSION['notice'] = $lng->{'Thank you for registration'};
			header("Location: ".$_PH_CONFIG['BASE_URL']);
			exit;
		}
	}

?>
<br />
<!-- TODO: Add javascript sanity checks before submitting -->
<form method="post" action="" enctype="multipart/form-data">
<table class="user-form">
<tr>
	<th colspan="2"><?php echo $lng->Registration; ?></td>
</tr>

<tr>
	<td><?php echo $lng->Username;?></td>
	<td><input type="text" name="username" maxlength="32" /></td>
</tr>
<tr>
	<td><?php echo $lng->Password;?></td>
	<td><input type="password" name="password" /></td>
</tr>
<tr>
	<td><?php echo $lng->{'Password again'};?></td>
	<td><input type="password" name="password_again" /></td>
</tr>
<tr>
	<td><?php echo $lng->{'E-mail'};?></td>
	<td><input type="text" name="email" /></td>
</tr>
<tr>
	<td><?php echo $lng->Avatar;?></td>
	<td><input type="file" name="avatar" /></td>
</tr>
<tr style='display: none;'>
	<td><?php echo $lng->{'Do not fill'};?></td>
	<td><input type="text" name="do_not_fill" /></td>
</tr>
<tr>
	<td colspan="2" align="center"><input type="submit" name="registration_submit" value="<?php echo $lng->Register;?>"></td>
</tr>
</table>
</form>