<?php
	defined("MODULE") or die(42);

	if (isset($_POST['add_topic']) && isset($_POST['topic']) && strlen($_POST['title']) > 2 && strlen($_POST['topic']) > 2 && $_USER) {

		$post_csrf_m = isset($_POST['csrf_magic'])?$_POST['csrf_magic']:'';
		$sess_csrf_m = isset($_SESSION['csrf_magic'])?$_SESSION['csrf_magic']:'';
		
		$title = htmlspecialchars($_POST['title']);
		$topic = htmlspecialchars($_POST['topic']);

//TODO: More sanity checks
		$db->e("INSERT INTO topic (forum_id, user_id, title, created_date) VALUES (?, ?, ?, strftime('%s','now'))", $fid, $_USER['id'], $title);
		$tid = $db->lastInsertId();
		echo ">>".$tid;
		$db->e("INSERT INTO post (topic_id, user_id, content, created_date) VALUES (?, ?, ?, strftime('%s','now'))", $tid, $_USER['id'], $topic);
		$db->e("UPDATE user SET posts_count = posts_count + 1 WHERE id=?", $_USER['id']);

		header("Location: ?mod=topic&t=".$tid);
	}


$_SESSION['csrf_magic'] = md5(mt_rand()); 
?>

<!-- TODO: Add javascript sanity checks before submitting -->
<form action="" method="post">
<input type="hidden" name='csrf_magic' value='<?php echo $_SESSION['csrf_magic']?>' />
<table width="100%" id="new_post">
	<tr>
		<th><?php echo $lng->{"New topic"}?></th>
	</tr>

	<tr>
		<td><input type="text" name="title" class="textfield" /></td>
	</tr>

	<tr>
		<td id="bbcodes">
			<ul id="emoticons_dropdown">
				<li>					
					<b>&#9786;</b>
					<div>
<?php
			$ic = 0;
			foreach($_PH_CONFIG['EMOTICON_CODES'] as $code => $icon) {
				echo (++$ic == 10)?'<br />'.(bool)$ic = 0:'';
				printf('<a style="font-size: 25px;" href="#" onclick="add_emoticon(\'%s\'); return false;">%s</a>', $code, $icon);
			}
?>
					</div>
				</li>
			</ul>
			|
			<button onclick="add_bb_tag('b'); return false;"><b>B</b></button>
			<button onclick="add_bb_tag('i'); return false;"><i>I</i></button>
			<button onclick="add_bb_tag('u'); return false;"><u>U</u></button>
			<button onclick="add_bb_tag('center'); return false;">center</button>
		</td>
	</tr>

	<tr>
		<td><textarea name="topic" cols="128" rows="16" id="content_area"></textarea></td>
	</tr>

	<tr>
		<td>
			<input type="submit" name="add_topic" value="<?php echo $lng->{'Add Topic'}?>"/>
			<input type="button" value="<?php echo $lng->{'Preview'}?>"/>
			<input type="button" onclick='document.location="?mod=forum&f=<?php echo $fid;?>";' value="<?php echo $lng->{'Cancel'}?>"/>
		<td>
	</tr>
</table>
</form>
