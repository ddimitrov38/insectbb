<?php
	defined("MODULE") or die(42);

	echo '<table class="overpost-table"><tr>';
	echo '<td class="forum-buttons" colspan="3"><a href="?mod=forum&f='.$fid.'&action=new">'.$lng->{'New topic'}.'</a></td>';
	echo '</tr></table>';

	$q = sprintf('
		SELECT 
			t.id, 
			t.title, 
			t.created_date, 
			t.count_views, 
			(SELECT COUNT(p.id) FROM post p where p.topic_id = t.id) as count_posts, 
			u.id as user_id, 
			u.user 
		FROM 
			topic t 
		LEFT JOIN 
			user u 
		ON 
			(u.id = t.user_id) 
		WHERE 
			t.forum_id = %d 
		ORDER BY t.created_date, t.id ASC;
		', 
		$fid
	);

	echo '<table class="forum-tbl">';
	printf('
		<tr>
			<th width="*">%s</th>
			<th width="15%%">%s</th>
			<th width="7%%">%s</th>
			<th width="7%%">%s</th>
			<th width="25%%">%s</th>
		</tr>',
		$lng->Topic,
		$lng->Author,
		$lng->Posts,
		$lng->Views,
		$lng->{'Last post'}
	);

	foreach ($db->query($q) as $topic) {
		$last_post_r = $db->q("SELECT u.user, t.title, u.id as user_id, p.created_date FROM post p LEFT JOIN user u ON (u.id = p.user_id) LEFT JOIN topic t ON (p.topic_id = t.id) WHERE t.id = ? ORDER BY p.created_date DESC LIMIT 1;", $topic['id']);

		$topic['last_post'] = sprintf("<i>%s</i><br />by <a href='?mod=user&id=%d'>%s</a>", 
					date("Y-m-d H:i:s", $last_post_r[0]['created_date']),
					$last_post_r[0]['user_id'],
					$last_post_r[0]['user']
		);
		printf("
			<tr>
				<td><a title='%s' href='?mod=topic&t=%d'>%s</span></td>
				<td><a href='?mod=user&id=%d'>%s</a></td>
				<td>%d</td>
				<td>%d</td>
				<td>%s</td>
			</tr>",
			$lng->{"Created on"}.' '.date("Y-m-d H:i:s", $topic['created_date']),
			$topic['id'],
			$topic['title'],
			$topic['user_id'],
			$topic['user'],
			$topic['count_posts'],
			$topic['count_views'],
			$topic['last_post']
		);
	}
	echo '</table>';
