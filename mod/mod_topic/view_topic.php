<?php
	defined("MODULE") or die(42);

	list(list($total_posts)) = $db->q("SELECT COUNT(id) FROM post WHERE topic_id = ?", $tid);
	$total_pages = ceil($total_posts / $_PH_CONFIG['POSTS_PER_PAGE']);
	$current_page = isset($_REQUEST['page'])?abs((int)$_REQUEST['page'] - 1):0;

	if ($current_page + 1 > $total_pages)
		$current_page = $total_pages - 1;

	$q = sprintf('
		SELECT 
			p.id,
			p.created_date,
			p.user_id,
			u.user,
			u.posts_count as user_posts_count,
			u.signature as user_signature,
			p.content
		FROM
			post p
		LEFT JOIN
			user u
		ON
			(u.id = p.user_id)
		WHERE
			p.topic_id = "%d"
		ORDER BY
			p.created_date, p.id 
		ASC
		LIMIT %d, %d
	', $tid, $current_page * $_PH_CONFIG['POSTS_PER_PAGE'], $_PH_CONFIG['POSTS_PER_PAGE']
	);

?>
<?php
// Pagging
	echo '<table class="overpost-table"><tr>';
	if ($total_pages > 1)
		pagging("?mod=topic&t=".$tid, $total_pages, $current_page);

	echo '<td class="forum-buttons" colspan="2"><a href="?mod=topic&t='.$tid.'&action=reply">'.$lng->{'Post reply'}.'</a></td>';
	echo '</tr></table>';
// END Pagging

	echo '<table class="post-tbl">';
	foreach ($db->query($q) as $post) {
?>
			<tr>
				<td rowspan='2' class="post-user-td" valign="top">
					<a href='?mod=user&id=<?php echo $post['user_id']?>'><?php echo $post['user']?></a>
					<div style="
margin: 0 auto;
background-image: url('?mod=avatar&id=<?php echo $post['user_id']?>'); width: 96px; height: 96px;border-radius: 10px;">
						<p class='user-avatar'></p>
					</div>
				</td>

				<td>

				<table style="width:100%; border-collapse: collapse;"><tr>
					<td style="padding-left: 10px; border: 0px; height: 40px;">
						<?php echo $lng->{'Posted on'}.' <i>'.date("Y-m-d H:i:s", $post['created_date'])?></i>
					</td>
					<td align="right" style="border: 0px;">
						<div class="quote-buttons">
							<a href="?mod=topic&t=<?php echo $tid;?>&action=delete&post=<?php echo $post['id'];?>" onclick="return confirm('<?php echo $lng->{'Are you sure you want to delete this post?'};?>');" title="<?php echo $lng->Delete?>" style="color: #dd0000;">&#10008;</a>
							<a href="?mod=topic&t=<?php echo $tid;?>&action=edit&post=<?php echo $post['id'];?>" title="<?php echo $lng->Edit?>" style="color: #dddd00;">&#9998;</a>
							<a href="?mod=topic&t=<?php echo $tid;?>&action=reply&quote=<?php echo $post['id'];?>" title="<?php echo $lng->Quote?>" style="color: #dd8800;">&#10078;</a>
						</div>
					</td>
				</tr>
				</table>

			</tr>

			<tr>
				<td class="post-content-td" colspan="2"><?php echo parse_post($post['content'], $_PH_CONFIG)?><br /><div class="user-signature"><?php if ($post['user_signature']) echo '<br /><hr />'.htmlspecialchars($post['user_signature'])?></div></td>
			</tr>

			<tr>
				<td style="border: 0px;line-height: 5px;"></td>
			</tr>
<?php
	}
	echo '</table>';

// Pagging
	echo '<table class="overpost-table"><tr>';
	if ($total_pages > 1)
		pagging("?mod=topic&t=".$tid, $total_pages, $current_page);

	echo '<td class="forum-buttons"><a href="?mod=topic&t='.$tid.'&action=reply">'.$lng->{'Post reply'}.'</a></td>';
	echo '</tr></table>';
// END Pagging
