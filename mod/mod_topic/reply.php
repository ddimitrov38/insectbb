<?php
	defined("MODULE") or die(42);

	if (isset($_POST['add_post']) && isset($_POST['post']) && strlen($_POST['post']) > 2 && $_USER) {

		$post_csrf_m = isset($_POST['csrf_magic'])?$_POST['csrf_magic']:'';
		$sess_csrf_m = isset($_SESSION['csrf_magic'])?$_SESSION['csrf_magic']:'';

//TODO: More sanity checks
		$db->e("INSERT INTO post (topic_id, user_id, content, created_date) VALUES (?, ?, ?, strftime('%s','now'))", $tid, $_USER['id'], htmlspecialchars($_POST['post']));
		$db->e("UPDATE user SET posts_count = posts_count + 1 WHERE id=?", $_USER['id']);

		header("Location: ?mod=topic&t=".$tid);
	}

$quote = NULL;
if (isset($_REQUEST['quote'])) {
	$quote_id = (int)$_REQUEST['quote'];

	$quote = $db->q("SELECT p.content, p.created_date, u.user FROM post p, user u WHERE p.id = ? AND p.user_id = u.id", $quote_id);

	if ($quote) {
		$quote_author = $quote[0]['user'];
		$quote_date = $quote[0]['created_date'];
		$quote = $quote[0]['content'];
	}
}

$_SESSION['csrf_magic'] = md5(mt_rand()); 
?>

<!-- TODO: Add javascript sanity checks before submitting -->
<form action="" method="post">
<input type="hidden" name='csrf_magic' value='<?php echo $_SESSION['csrf_magic']?>' />
<table width="100%" id="new_post">
	<tr>
		<th><?php echo $lng->{"Adding post to"}.' ['.$topic_info[0]['title'].']'?></th>
	</tr>

	<tr>
		<td id="bbcodes">
			<ul id="emoticons_dropdown">
				<li>					
					<b>&#9786;</b>
					<div>
<?php
			$ic = 0;
			foreach($_PH_CONFIG['EMOTICON_CODES'] as $code => $icon) {
				echo (++$ic == 10)?'<br />'.(bool)$ic = 0:'';
				printf('<a style="font-size: 25px;" href="#" onclick="add_emoticon(\'%s\'); return false;">%s</a>', $code, $icon);
			}
?>
					</div>
				</li>
			</ul>
			|
			<button onclick="add_bb_tag('b'); return false;"><b>B</b></button>
			<button onclick="add_bb_tag('i'); return false;"><i>I</i></button>
			<button onclick="add_bb_tag('u'); return false;"><u>U</u></button>
			<button onclick="add_bb_tag('center'); return false;">center</button>
		</td>
	</tr>

	<tr>
		<td><textarea name="post" cols="128" rows="16" id="content_area"><?php if ($quote) printf('[quote=%s %s]%s[/quote]', $quote_author, date("Y-m-d H:i:s", $quote_date), $quote);?></textarea></td>
	</tr>

	<tr>
		<td>
			<input type="submit" name="add_post" value="<?php echo $lng->{'Add Post'}?>"/>
			<input type="button" value="<?php echo $lng->{'Preview'}?>"/>
			<input type="button" onclick='document.location="?mod=topic&t=<?php echo $tid;?>";' value="<?php echo $lng->{'Cancel'}?>"/>
		<td>
	</tr>
</table>
</form>
