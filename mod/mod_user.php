<?php
	defined("MODULE") or die(42);

	$act = isset($_REQUEST['action'])?$_REQUEST['action']:'default';

// Fast user login
	if ($act == 'login') {
		$username = $_POST['user'];
		$password = $_POST['pass'];

		if ($usr->login($username, $password))
			$_SESSION['notice'] = $lng->{'Access granted. Welcome!'};
		else
			$_SESSION['error'] = $lng->{'ACCESS DENIED'};

		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

// Logout
	if ($act == 'logout') {
		$_SESSION['user'] = null;
		session_destroy();

		header("Location: ".$_SERVER['HTTP_REFERER']);
		exit;
	}

// Register
	if ($act == 'create')
		require_once("mod_user/create.php");
