<?php
	defined('MODULE') or die(42);
	if ($_USER) {
?>
	<table class="user-form" style="width: 500px; margin-top: 10px; margin-bottom: 10px;">
	<tr>
		<th colspan="2"><?php echo $lng->{'User setings'};?></th>
	</tr>

	<tr>
		<td><?php echo $lng->{'Avatar'}?></td>
		<td><input type="file" name="avatar" /></td>
	</tr>

	<tr>
		<td><?php echo $lng->{'Signature'}?></td>
		<td><textarea name="signature"></textarea></td>
	</tr>
	</table>
<?php
	} else {
		merror($lng->{'Not logged in'});
	}