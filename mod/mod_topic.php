<?php
	defined("MODULE") or die(42);

	$tid = isset($_REQUEST['t'])?(int)$_REQUEST['t']:0;
	$act = isset($_REQUEST['action'])?$_REQUEST['action']:'default';

	$topic_info = $db->q('SELECT t.user_id, u.user, t.title, t.created_date FROM topic t LEFT JOIN user u ON (t.user_id = u.id) WHERE t.id = ?', $tid);

	if (!$topic_info) {
			merror($lng->{'Topic not found!'});
	} else {
		switch ($act) {
			case 'reply':
				if (!$_USER) {
					merror($lng->{'Not logged in'});
					break;
				}

				require_once("mod_topic/reply.php");
			break;

			default:
				require_once("mod_topic/view_topic.php");
			break;
		}
	}
