--- SAMPLE DATA ---
INSERT INTO section (title, permissions, position) VALUES ('General', 0, 0);
INSERT INTO forum (section_id, title, description, position) VALUES (1, 'About', 'About the forum', 0);
INSERT INTO forum (section_id, title, description, position) VALUES (1, 'Test 1', 'Test forum 1', 1);
INSERT INTO forum (section_id, title, description, position) VALUES (1, 'Test 2', 'Test forum 2', 2);
INSERT INTO user (user, pass, email, avatar, signature, created_date, last_online_date) VALUES ('admin', '$2a$08$VKtJWG2F6gVexKS7DYX3wObvWuSTjjL5wThsxa2iplu77OfZ3wNqq ','admin@forum.com', '', 'Zeroes and ones will take us there...','1303374193', '1303374193');
INSERT INTO topic (forum_id, user_id, title, created_date) VALUES (1, 1, 'Test topic 1', '1303374113');
INSERT INTO topic (forum_id, user_id, title, created_date) VALUES (1, 1, 'Test topic 2', '1303374313');
INSERT INTO post (topic_id, user_id, content, created_date) VALUES (1, 1, 'This is test content 1', '1303374313');
INSERT INTO post (topic_id, user_id, content, created_date) VALUES (2, 1, 'This is test content 2', '1303374313');
