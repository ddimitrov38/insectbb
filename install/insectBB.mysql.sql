create table if not exists forum (
  id int(11) not null auto_increment,
  section_id int(11) not null default '0',
  title varchar(255) collate utf8_bin not null,
  description text collate utf8_bin not null,
  position int(11) not null default '0',

  primary key (id),
  key section_id (section_id,position)
) ENGINE=InnoDB default CHARSET=utf8 collate=utf8_bin;

create table if not exists post (
  id int(11) not null auto_increment,
  topic_id int(11) not null,
  user_id int(11) not null,
  content text collate utf8_bin not null,
  created_date timestamp not null default CURRENT_TIMESTAMP,

  primary key (id),
  key topic_id (topic_id,user_id)
) ENGINE=InnoDB default CHARSET=utf8 collate=utf8_bin;

create table if not exists section (
  id int(11) not null auto_increment,
  title varchar(255) collate utf8_bin not null,
  permissions int(11) not null,
  position int(11) not null,

  primary key (id),
  key permissions (permissions,position)
) ENGINE=InnoDB default CHARSET=utf8 collate=utf8_bin;

create table if not exists settings (
  id int(11) not null auto_increment,
  `name` varchar(64) collate utf8_bin not null,
  `value` text collate utf8_bin not null,
  description text collate utf8_bin not null,

  primary key (id)
) ENGINE=InnoDB default CHARSET=utf8 collate=utf8_bin;

create table if not exists topic (
  id int(11) not null auto_increment,
  forum_id int(11) not null,
  user_id int(11) not null,
  title varchar(255) collate utf8_bin not null,
  created_date timestamp not null default CURRENT_TIMESTAMP,
  count_views int(11) not null,

  primary key (id),
  key forum_id (forum_id,user_id,count_views)
) ENGINE=InnoDB default CHARSET=utf8 collate=utf8_bin;

create table if not exists `user` (
  id int(11) not null auto_increment,
  `user` varchar(32) collate utf8_bin not null,
  pass varchar(255) collate utf8_bin not null,
  email varchar(128) collate utf8_bin not null,
  avatar blob not null,
  signiture text collate utf8_bin not null,
  perms int(11) not null,
  created_date timestamp not null default CURRENT_TIMESTAMP,

  primary key (id),
  key `user` (`user`,group_id)
) ENGINE=InnoDB default CHARSET=utf8 collate=utf8_bin;
