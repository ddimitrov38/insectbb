--- 20.12.2011 ---

CREATE TABLE IF NOT EXISTS forum (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	section_id INTEGER NOT NULL default 0,
	title TEXT,
	description TEXT,
	position INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS post (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	topic_id INTEGER NOT NULL default 0,
	user_id INTEGER NOT NULL default 0,
	content TEXT,
	created_date TEXT
);

CREATE TABLE IF NOT EXISTS section (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	title TEXT,
	permissions INTEGER NOT NULL default 0,
	position INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS settings (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name TEXT, value TEXT,
	description TEXT
);

CREATE TABLE IF NOT EXISTS topic (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	forum_id INTEGER NOT NULL default 0,
	user_id INTEGER NOT NULL default 0,
	title TEXT,
	created_date TEXT,
	count_views INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS user (
	id INTEGER PRIMARY KEY, 
	perms INTEGER NOT NULL default 0, 
	user TEXT, 
	pass TEXT, 
	email TEXT, 
	avatar BLOB, 
	signature TEXT, 
	created_date TEXT, 
	created_ip TEXT,
	last_online_date TEXT, 
	posts_count INTEGER NOT NULL default 0
);

CREATE INDEX forum_position ON forum (position);
CREATE INDEX forum_section_id ON forum (section_id);
CREATE INDEX post_forum_id ON post (topic_id);
CREATE INDEX post_user_id ON post (user_id);
CREATE INDEX section_position ON section (position);
CREATE INDEX topic_forum_id ON topic (forum_id);
CREATE INDEX topic_user_id ON topic (user_id);
